import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
	const { _id, name, description, price } = courseProp;
	return(
			<Card >
				<Card.Body>
					<Card.Title><h2>{name}</h2></Card.Title>
					<Link className="btn btn-primary" to={`/courses/${_id}`}> Details </Link>

				</Card.Body>
			</Card>

		)
}



CourseCard.propTypes = {
	//The 'shape' method is used to check if a prop object conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
